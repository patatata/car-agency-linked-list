// car_agency_linked_list.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
//struktury danych - zmienna, struktury, tablice. istnieja bardziej skomplikowane dane ktore ukladaja dane w odpowiedni sposob
//stos, lista, kolejka, graf, drzewo, 

/*
Agencja samochodowa zawiera rozne rodzaje samochodow. Kazdy klient wydaje zamowienie
na zakup samochodu. Pracownicy beda wyszukiwac wymaganego samochodu na zadanie klienta 
w kolejnosci (kto pierwszy ten lepszy). 
System agencji samochodowych ma nastepujace wymagania:
1) Istnieje wiele rodzajow samochodow w spisie agencji samochodowej co najmniej 10.
Moga miec wiecej niz jeden samochod z kazdego rodzaju
2) Klienci sa zorganizowani tak, ze kto pierwszy sie zglosil ten ma pierwszenstwo w wyborze samochodu
3) Pobieranie danych z ostatniego sprzedanego samochodu
 
Program powinien skladac sie z nastepujacych elementow:
1) Klasa implementujaca liste obejmujaca nastepujace funkcje:
a) InsertatHead
b) InsertatTail
c) DeletefromHead
d) DeletefromTail
e) DeleteSpecificValue
2) Klasa implementujaca stos dziedziczony z listy klasy i majaca nastepujace funkcje:
a) push - dodaje element na wierzcholek stosu
b) pop - usuwa element z wierzcholka stosu
c) top
3) Klasa implementujaca kolejke odziedziczona z klasy lista i majaca nastepujace funkcje:
a) Enqueue (zamocuj) //dodanie elementu na koniec kolejki
b) Dequeue (usunac) - usuniecie elementu z przodu kolejki
c) Front (przod) - sprawdzenie co jest na poczatku kolejki

Kaza klasa powinna miec konstruktor i destruktor 
Nazwy samochodow sa przechowywane na liscie. Zamowienia klientow sa przechowywane w kolejce.
Nalezy wziac samochod z poczatku kolejki odszukac go na liscie a nastepnie usunac i umiescic
sprzedany samochod w stosie, aby moc odzyskac ostatnio sprzedany samochod. 

*/

//2 punkt to stos czyli LIFO (last inf first out 
#include "LinkedList.h"
#include "Stack.h"
#include "Queue.h"

void completeOrder();
void showLastSold();


LinkedList *list = new LinkedList();
Queue *queue1 = new Queue();
Stack *stack = new Stack();

int main()
{
	int x = 1;
	string choice = "";
	cout << "Witamy w aplikacji Agencja samochodowa" << endl << endl;
	cout << "Lista samochodow w agencji" << endl;
	list->insertAtHead("volvo XC90");
	list->insertAtHead("mercedes A");
	list->insertAtHead("audi A4");
	list->insertAtHead("audi A3");
	list->insertAtHead("citroen C4");
	list->printList();

	
	cout << "Wyswietl wszystkie zamowienia" << endl;
	queue1->Enqueue("audi A3");
	queue1->Enqueue("volvo XC90");
	queue1->Enqueue("citroen C4");
	queue1->printList();

	while (x)
	{
		cout << "1. Zrealizuj zamowienie pierwsze w kolejce" << endl;
		cout << "2. Wyswietl informacje o ostatnio sprzedanym samochodzie" << endl;
		cout << "3. Exit" << endl;
		cin >> choice;
		if (choice == "1") {
			completeOrder();
		}
		else if (choice == "2") {
			showLastSold();
		}
		else if (choice == "3") {
			x = 0;
		}
		else {

		}
	}


	getchar();
	getchar();
    return 0;
}


void completeOrder() {
	cout << "First in queue: " << queue1->Front()  <<endl;
	list->deleteSpecificObj(queue1->Front());
	stack->push(queue1->Front());
	queue1->deleteFromHead();
	cout << " Lista samochodow teraz: " << endl;
	list->printList();
}

void showLastSold() {
	stack->top();
}


//kasowanie wszystkich obiektow zaimplementowac rozwiazanie 