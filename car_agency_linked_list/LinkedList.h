#pragma once
#include "stdafx.h"
#include <iostream>

using namespace std;

struct node {
	string name;
	node *next;  //wskaznik do struktury
};

class LinkedList {
private:

	node *head, *tail;
public:
	LinkedList();
	~LinkedList();

	void insertAtTail(string name);
	void insertAtHead(string name);
	void deleteFromHead();
	void deleteFromTail();
	bool deleteSpecificObj(string name);
	void printList();
	string printTop();

};
