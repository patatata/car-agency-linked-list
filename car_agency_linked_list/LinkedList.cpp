#include "stdafx.h"
#include "LinkedList.h"
#include <string>

LinkedList::LinkedList() {
	head = nullptr;
	tail = nullptr;
}

LinkedList::~LinkedList() {
	cout << "List deleted" << endl;
}

void LinkedList::insertAtTail(string name) { //w momencie dodawania tego elementu bedzie on ostatnim
	node *temp = new node; //stworzenie wskaznika na nowy element
	temp->name = name;
	temp->next = nullptr;

	if (head == nullptr) {
		head = temp; //pierwszy powstaly element bedzie glowa
		tail = temp;
		temp = nullptr;
	}
	else {
		tail->next = temp; //za ostatnim node'em ustawia sie adres nowego node'a
		tail = temp; //teraz nowy node jest ostatnim nodem
	}

	//delete temp;
	//mix insertathead i insertat tail sprawdzic dlaczego zglasza sie wyjatek

}



void LinkedList::insertAtHead(string name) {
	node *temp = new node;
	temp->name = name;
	if (this->head == nullptr) {
		this->head = temp;
		this->tail = temp;
		temp->next = nullptr;
	}
	else {
		temp->next = this->head; //temp
		this->head = temp;
	}

}

void LinkedList::deleteFromHead() {
	node *temp = new node;
	temp = head;
	head = head->next;
	delete temp;
}

void LinkedList::deleteFromTail() {
	node *current = new node;
	node *previous = new node;
	current = head;
	while (current->next != nullptr) {
		previous = current;
		current = current->next;
	}
	tail = previous;
	previous->next = nullptr; //ustawienie kolejnego elementu jako tail
	//delete current;
	
	//tail = current;
	//previous = tail->next;
	//delete current;
	//delete previous;

}

bool LinkedList::deleteSpecificObj(string name) {
	node *current = new node;
	node *previous = new node;
	current = head;
	while (current->name != name) {
		if (current == tail) {
			cout << "There is no such object" << endl;
			return 0;
		}
		previous = current;
		current = current->next;
	}
	previous->next = current->next; //to co bylo przed bedzie teraz nastepne
	return 1;
}


void LinkedList::printList() {
	int counter = 0;
	node *temp = this->head;
	while (temp != nullptr)
	{
		cout << ++counter << ". "<< temp->name << endl;
		temp = temp->next;
	}

	cout << "\n";
}

string LinkedList::printTop() {
	return head->name;
}






