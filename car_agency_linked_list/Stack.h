#pragma once
#include "stdafx.h"
#include "LinkedList.h"

//stos - tak jak np. stos ksiazek, mam dostep tylko wierzcholka, aby dobrac sie do innej ksiazki,
//musze wyjac najpierw ksiazki z gory. Jezeli potrzebujemy miec szybki dostep do danych ostatnio uzywanych stos jest ok

class Stack : public LinkedList {
public:
	Stack();
	~Stack();

	void push(string name);
	void pop();
	void top();

};

