#pragma once
#include "LinkedList.h"
#include <string>



//kolejka - pierwszy wchodzacy element to pierwszy wychodzacy, jest to kolejka typu FIFO
class Queue: public LinkedList {
public: 
	Queue();
	~Queue();

	void Enqueue(string name);
	void Dequeue();
	string Front();

};